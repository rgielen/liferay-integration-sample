package com.liferay.docs.guestbook.service.impl;

import com.liferay.docs.guestbook.EntryEmailException;
import com.liferay.docs.guestbook.EntryMessageException;
import com.liferay.docs.guestbook.EntryNameException;
import com.liferay.docs.guestbook.model.Entry;
import com.liferay.docs.guestbook.service.EntryLocalServiceUtil;
import com.liferay.docs.guestbook.service.base.EntryLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.ResourceLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.model.AssetLinkConstants;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;

import java.util.Date;

/**
 * The implementation of the entry local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.docs.guestbook.service.EntryLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Rich Sezov
 * @see com.liferay.docs.guestbook.service.base.EntryLocalServiceBaseImpl
 * @see com.liferay.docs.guestbook.service.EntryLocalServiceUtil
 */
public class EntryLocalServiceImpl extends EntryLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.liferay.docs.guestbook.service.EntryLocalServiceUtil} to access the entry local service.
     */

    public Entry addEntry(long userId, long guestbookId, String name,
                          String email, String message, ServiceContext serviceContext) throws PortalException, SystemException {
        long groupId = serviceContext.getScopeGroupId();

        User user = userPersistence.findByPrimaryKey(userId);

        Date now = new Date();

        validate(name, email, message);

        long entryId = counterLocalService.increment();

        Entry entry = createAndSaveEntry(userId, guestbookId, name, email, message, serviceContext, groupId, user, now, entryId);

        resourceLocalService.updateResources(user.getCompanyId(), groupId,
                Entry.class.getName(), entryId,
                serviceContext.getGroupPermissions(),
                serviceContext.getGuestPermissions());

        AssetEntry assetEntry = assetEntryLocalService.updateEntry(userId,
                groupId, entry.getCreateDate(), entry.getModifiedDate(),
                Entry.class.getName(), entryId, entry.getUuid(), 0,
                serviceContext.getAssetCategoryIds(),
                serviceContext.getAssetTagNames(), true, null, null, null,
                ContentTypes.TEXT_HTML, entry.getMessage(), null, null, null,
                null, 0, 0, null, false);

        assetLinkLocalService.updateLinks(userId, assetEntry.getEntryId(),
                serviceContext.getAssetLinkEntryIds(),
                AssetLinkConstants.TYPE_RELATED);

        Indexer indexer = IndexerRegistryUtil.nullSafeGetIndexer(Entry.class);

        indexer.reindex(entry);


        return entry;
    }

    private Entry createAndSaveEntry(long userId, long guestbookId, String name, String email, String message, ServiceContext serviceContext, long groupId, User user, Date now, long entryId) throws SystemException {
        Entry entry = entryPersistence.create(entryId);

        entry.setUuid(serviceContext.getUuid());
        entry.setUserId(userId);
        entry.setGroupId(groupId);
        entry.setCompanyId(user.getCompanyId());
        entry.setUserName(user.getFullName());
        entry.setCreateDate(serviceContext.getCreateDate(now));
        entry.setModifiedDate(serviceContext.getModifiedDate(now));
        entry.setExpandoBridgeAttributes(serviceContext);
        entry.setGuestbookId(guestbookId);
        entry.setName(name);
        entry.setEmail(email);
        entry.setMessage(message);
        entry.setStatus(WorkflowConstants.STATUS_DRAFT);

        entryPersistence.update(entry);
        return entry;
    }


    protected void validate (String name, String email, String entry) throws PortalException {
        if (Validator.isNull(name)) {
            throw new EntryNameException();
        }

        if (!Validator.isEmailAddress(email)) {
            throw new EntryEmailException();
        }

        if (Validator.isNull(entry)) {
            throw new EntryMessageException();
        }
    }

    public Entry deleteEntry(long entryId, ServiceContext serviceContext)
            throws PortalException, SystemException {

        Entry entry = getEntry(entryId);

        resourceLocalService.deleteResource(serviceContext.getCompanyId(),
                Entry.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
                entryId);
        entry = deleteEntry(entryId);

        return entry;
    }


    }
