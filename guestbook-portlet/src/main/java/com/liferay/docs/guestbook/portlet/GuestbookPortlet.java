package com.liferay.docs.guestbook.portlet;

import com.liferay.docs.guestbook.model.Entry;
import com.liferay.docs.guestbook.service.EntryServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class GuestbookPortlet extends MVCPortlet {

    public void addEntry(ActionRequest request, ActionResponse response)
            throws PortalException, SystemException {

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Entry.class.getName(), request);

        String userName = ParamUtil.getString(request, "name");
        String email = ParamUtil.getString(request, "email");
        String message = ParamUtil.getString(request, "message");
        String guestbookName = ParamUtil.getString(request, "guestbookName");
        long guestbookId = ParamUtil.getLong(request, "guestbookId");
        long entryId = ParamUtil.getLong(request, "entryId");

        if (entryId > 0) {
            try {
                EntryServiceUtil.updateEntry(serviceContext.getUserId(),
                        guestbookId, entryId, userName, email, message,
                        serviceContext);

                SessionMessages.add(request, "entryAdded");

                response.setRenderParameter("guestbookName", guestbookName);
            } catch (Exception e) {
                SessionErrors.add(request, e.getClass().getName());

                PortalUtil.copyRequestParameters(request, response);

                response.setRenderParameter("mvcPath",
                        "/html/guestbook/edit_entry.jsp");
            }
        } else {
            try {
                EntryServiceUtil.addEntry(serviceContext.getUserId(),
                        guestbookId, userName, email, message, serviceContext);

                SessionMessages.add(request, "entryAdded");

                response.setRenderParameter("guestbookName", guestbookName);
            } catch (Exception e) {
                SessionErrors.add(request, e.getClass().getName());

                PortalUtil.copyRequestParameters(request, response);

                response.setRenderParameter("mvcPath",
                        "/html/guestbook/edit_entry.jsp");
            }
        }
    }
}
